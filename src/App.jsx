import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

// Commento breve

/*
  Commento lungo 
  Commento lungo 
  Commento lungo 
  Commento lungo 
*/

const useCallApi = () => {
  const [data, setData] = useState(null)

  const callApi = async () => {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/1')
    const data = await response.json()
    setData(data)
  }

  return { data, callApi }
}

function App() {
  const [count, setCount] = useState(0)
  const [showSquare, setShowSquare] = useState(false)
  const { data, callApi } = useCallApi()

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <button onClick={() => setShowSquare(!showSquare)}>
          Show square
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
        <button onClick={callApi}>Call API</button>
        {data 
          ? <pre>{JSON.stringify(data, null, 2)}</pre> 
          : null}
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
      {showSquare ? <Square /> : null}
    </div>
  )
}

const Square = () => (
  <div style={{height: 50, width: 50, backgroundColor: 'red'}} />
)

export default App
